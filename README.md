S3 Testing Tools
================

Tools for working with S3 storage

## Requirements

**Python Libraries:**

* boto3
* [devilparser](https://gitlab.oit.duke.edu/devil-ops/devilparser)

**Configuration File:**

These scripts require a config file `~/.s3.yaml`, with the server name you wish to connect to, and the access and secret keys for that connection.  You may also specify a different config file on the command line with the `--config-file` option.

_Config File Format:_

```
---
  'https://server_name':
    access_key: vvndejcufigvckhblndvdiecdvblertcjercijeeihkn
    secret_key: vvndejcufigvflfgcrebbiddgnejinbvvblghntlucji
```

