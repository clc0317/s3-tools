#!/usr/bin/env python3

import boto3
import boto.s3
import sys
import os
import argparse
import errno
from boto.s3.key import Key
from devilparser import rcfile
import botocore.vendored.requests
from botocore.vendored.requests.packages.urllib3.exceptions import InsecureRequestWarning
botocore.vendored.requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def parse_arguments():
    """
    Parse the command line arguments
    """

    parser = argparse.ArgumentParser(description=" ".join([
        "Command line tool for recursively uploading files in a directory",
        "to an S3 bucket."
        ]))
    parser.add_argument('-f', '--config-file', action='store',
        default=os.path.expanduser("~/.s3.yaml"),
        help="Set the config file location (default: ~/.s3creds")
    parser.add_argument('-b', '--bucket', action='store',
        required=True,
        help=" ".join(["The name of the bucket to upload to.",
        " A new bucket will be created if it does not exist."
        ]))
    subparsers = parser.add_subparsers(dest='command')
    subparsers.required = True

    put = subparsers.add_parser('RAW', help='get raw bucket info')
    put = subparsers.add_parser('PUT', help='upload files from a directory')
    put.add_argument('-a', '--acl', action='store',
        default='private',
        choices=['private',
                 'public-read',
                 'public-read-write',
                 'authenticated-read'],
        help=" ".join([
        "ACLs to give to the bucket if it is created",
        "Ignored if the bucket already exists."
        ]))
    put.add_argument('-s', '--src', action='store',
        help="Source directory to upload files from, recursively")
    get = subparsers.add_parser('GET', help='download files from a bucket')
    get.add_argument('-d', '--dest', action='store',
         help="Destination directory to download files to")

    return parser.parse_args()


def s3_url():
    if 'S3_URL' in os.environ.keys():
        s3_url = os.environ['S3_URL']
        print("Connecting to {}".format(s3_url))
    else:
        print("Set the 'S3_URL' variable in your environment to connect")
        sys.exit(1)

    return s3_url


def get_cred_info(config_file, s3_url):
    config_file=os.path.expanduser("~/.s3.yaml")
    if not os.path.exists(config_file):
            sys.exit("%s not found" % config_file)
    configinfo = rcfile.parse(config_file, s3_url).contents()
    creds = {}
    creds['access_key'] = configinfo['access_key']
    creds['secret_key'] = configinfo['secret_key']
    # Dervilparser bug
    # File:
    # "/home/chris/.local/lib/python3.6/site-packages/devilparser/rcfile.py",
    # line 13, in __init__
    # if value[0:2] == '$ ':
    # TypeError: 'bool' object is not subscriptable
    # creds['verify'] = configinfo['verify']
    creds['verify'] = False
    return creds


def conn(config_file):
    endpoint_url = s3_url()
    creds = get_cred_info(config_file, endpoint_url)
    access_key = creds['access_key']
    secret_key = creds['secret_key']

    if 'verify' in creds.keys():
        verify = creds['verify']
    else:
        verify = None

    session = boto3.session.Session()

    s3_client = session.client(
        service_name='s3',
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        verify=verify,
        endpoint_url=endpoint_url
    )

    return s3_client


def get_raw_bucket_info(client):
    response = client.list_buckets()

    return response


def check_buckets(client):
    response = get_raw_bucket_info(client)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        buckets = response['Buckets']
    else:
        raise

    return buckets


def get_bucket_names(client):
    buckets = check_buckets(client)
    bucket_names = [bucket['Name'] for bucket in buckets]

    return bucket_names


def create_bucket(client, bucket, acl='private'):
    response = client.create_bucket(Bucket=bucket, ACL=acl)

    return response


def delete_bucket(client, bucket):
    response = client.delete_bucket(Bucket=bucket)

    return response


def upload_file(client, bucket, file_object):
    directory, filename = os.path.split(file_object)
    print("Uploading {} to {} as {}".format(file_object, bucket, filename))

    response = client.upload_file(file_object, bucket, filename)
    return response


def download_file(client, bucket, key, output_path):
    output = os.path.join('/', output_path, key)
    print("Downloading {}".format(output))
    client.download_file(bucket, key, output)


def get_s3_keys(client, bucket):
    """
    Return a list of keys (Object Names) from the bucket
    """
    keys = []
    response = client.list_objects_v2(Bucket=bucket)
    if 'Contents' in response.keys():
        for obj in response['Contents']:
            keys.append(obj['Key'])
    else:
        keys = []

    return keys


def get_files_to_upload(directory):
    """
    Find any files in the upload directory, and it's subdirectories
    """
    uploads = []
    for root, directory, files in os.walk(directory):
        for file_object in files:
            uploads.append(os.path.join('/', root, file_object))

    return uploads


def find_or_create_destination(directory):
    """
    Try to create the destination directory if it doesn't exist;
    and try to handle the race condition of "created between check
    and create"
    """
    try:
      os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def main():
    args = parse_arguments()

    # Global vars
    config_file = args.config_file
    bucket = args.bucket

    command = args.command

    # Connect to the server
    client = conn(config_file)

    if command == 'PUT':
        acl = args.acl
        source_directory = args.src

        files_to_upload = get_files_to_upload(source_directory)

        bucket_names = get_bucket_names(client)
        if not bucket in bucket_names:
            print('Bucket {} does not exist; creating'.format(bucket))
            create_bucket(client, bucket, acl)
            # Refresh
            print(get_bucket_names())

        # Check keys first?  Don't override?  Do override?
        # keys = get_s3_keys(client, bucket)
        for file_object in files_to_upload:
            upload_file(client, bucket, file_object)

        # Refresh list
        keys = get_s3_keys(client, bucket)
        if len(keys) > 0:
            print("Current Object Keys:")
            for key in keys:
                print("  {}".format(key))

    if command == 'GET':
        destination_directory = args.dest
        find_or_create_destination(destination_directory)

        keys = get_s3_keys(client, bucket)
        for key in keys:
            download_file(client, bucket, key, destination_directory)

    if command == 'RAW':
        print(get_raw_bucket_info(client))


if __name__ == "__main__":
    sys.exit(main())
